# fix images by mimetype



## Getting started

This file can search multiple directory's recursively for files that
have mismatching metadata and filename dot extensions and will attempt to fix
them by renaming the file to match it's metadata. It will default to your $HOME
folder which im debating if it's a good idea or not. I have found some files
in $HOME/.steam that I/you(?) might not want to have change. That is why I was 
thinking I might have to make a GUI? 

## Usage:

    $ python fix_image_by_mimetype.py /your/directory /your/directory2

