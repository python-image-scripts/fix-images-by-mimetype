#!/usr/bin/env python3
"""\n  This file can search multiple directory's recursively for files that have
mismatching metadata and filename dot extensions and will attempt to fix them
by renaming the file to match it's metadata. It will default to your $HOME
folder which im debating if it's a good idea or not. I have found some files
in $HOME/.steam that I/you(?) might not want to have change. That is why I was 
thinking I might have to make a GUI? 

 Usage:

    $ python fix_image_by_mimetype.py /your/directory /your/directory2

"""

from sys import argv, exit as sExit
from os import walk, remove
from os.path import splitext, expanduser, isdir, join as oJoin
from PIL import Image
from shutil import copyfile

jpgs = ('.jpg', '.jpeg')

formats = ('.png', '.bmp', '.webm', '.gif') + jpgs


def dir_borked_img_search(searchDir=expanduser('~')):
    """ This function searches the entire directory provided or the users $HOME 
directory; for files that are in our allowed formats tuple and sends it to
fix_by_mimetype() for processing. """
    for path, _, files in walk(searchDir):
        for file_ in files:
            if splitext(file_)[1] in formats:
                try:
                    im = Image.open(oJoin(path, file_))
                    fix_by_mimetype(path, file_, im)
                except OSError:
                    pass


def fix_by_mimetype(path, file_, im):
    """ This function will look at the image mimetype and the filenames dot 
extension making sure they are a match. If not a match this function replaces
the filename to reflect it's metadata. """
    ext = splitext(file_)[1][1:]
    if ext != im.format.lower():
        ## We know that the dot extension is not matching the metadata.
        if ext not in jpgs and '.' + im.format.lower() not in jpgs:
            ## We know that the mimetypes aren't mismatching pairs of JPEGS.
            ## This file needs fixed.
            print(f'\next: {ext}\nMime: {im.format.lower()}')
            print(f'path: {oJoin(path, file_)}')
            ## Try: see if we have permission to write to file system.
            try:
                ## this copy is there for gui testing making sure I have some
                ## files to work with if thats what happens.
                # copyfile(oJoin(path, file_),
                #           oJoin(expanduser('~'), 'Desktop',
                #                 'Test_Imgs', file_))
                im.save(oJoin(path, 
                              splitext(file_)[0] + '.' + im.format.lower()), 
                        im.format.lower())

                remove(oJoin(path, file_))
            except PermissionError as error_:
                raise error_
    im.close()


if __name__ == '__main__':

    if True in (True for x in ('-h', '--help') if x in argv[1:]):
        ## Let's do "Get Help"!
        sExit(__doc__)

    ## Clean out the keyword argument variables and separators.
    args = [x for x in argv[1:] if '=' not in x and ':' not in x]

    # print(f" args: {args}\n")

    if args != []:
        for arg in args:
            if isdir(arg):
                dir_borked_img_search(arg)
    else:
        ## Uncomment below to enable default $HOME directory search.
        # dir_borked_img_search()

        ## Dont forget to remove pass.
        pass
